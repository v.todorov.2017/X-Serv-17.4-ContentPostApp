#!/usr/bin/python3

import webapp

class contentApp(webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Ruta principal',
               '/noticias': 'Bienvenido a la seccion de noticias',
               '/prueba': 'Esto es una prueba'
               }

    def parse(self, request):
        """Return the resource name (including /)"""
        recurso = request.split(' ', 2)[1]
        print("Recurso:", recurso)
        cuerpo = request.split('=')[-1]
        print("Cuerpo: ", cuerpo)
        metodo = request.split(' ', 2)[0]
        print("Metodo:", metodo)

        return recurso, cuerpo, metodo

    def process(self, parsedRequest):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """
        recurso, cuerpo, metodo = parsedRequest

        formulario = "<p>" + "<form action='' method='POST'><p>" \
                     + "Escriba el contenido que desee para esta ruta : <input name = 'cuerpo'>" \
                     + "<input type='submit' value='Enviar' />" \
                     + "</body></html>"

        if metodo == "GET":
            if recurso in self.content.keys():
                httpCode = "200 OK"
                htmlBody = '<html><body><meta charset="UTF-8"/>' \
                           + "<p>La ruta " + recurso + " existe y contiene: " + self.content[recurso] + "<p>" \
                           + formulario + '<body><html>'
            else:
                httpCode = "404 Not Found"
                htmlBody = '<html><body><meta charset="UTF-8"/>' \
                    + "<p>La ruta " + recurso + " no existía pero la acabamos de crear<p>" \
                    + formulario + '<body><html>'
        else:
            self.content[recurso] = cuerpo
            print(self.content)
            if recurso in self.content.keys():
                httpCode = "200 OK"
                htmlBody = '<html><body><meta charset="UTF-8"/>' \
                           + "<p>La ruta " + recurso + " ahora contiene: " + self.content[recurso] + "<p>" \
                           + formulario + '<body><html>'
            else:
                httpCode = "404 Not Found"
                htmlBody = '<html><body><meta charset="UTF-8"/>' \
                    +"<p>La ruta" + recurso + " no existía pero la acabamos de crear<p>" \
                    + formulario + '<body><html>'

        return (httpCode, htmlBody)

if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)
